import React, { Component } from 'react';
import {
    Tab, Container, Header, Segment, Form, Icon, Button
} from 'semantic-ui-react'
import update from 'immutability-helper';
import CursoList from '../curso/CursoList';
import OfertaList from '../oferta/OfertaList';
import UnidadeEnsinoList from '../unidade/UnidadeEnsinoList';
import IESList from '../ies/IESList';
import EmecHeader from './EmecHeader';


class Emec extends Component{

    state = {
        loading: false,
        data : [],
        form: {
            codigo: "",
            nome: "",
            invalid: true
        }
    };

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });

        form.invalid = !form.codigo && !form.nome;
        this.setState({form: form, submit: false});
    };

    handleSubmit = () => {
        this.setState({submit: true});
    };

    handleClear = () => {
        if(!this.state.form.invalid) {
            let state = update(this.state, {
                submit: { $set: false },
                clear: { $set: true }
            });
            state.form = {
                codigo: "",
                nome: "",
                invalid: true,
            };

            this.setState(state);
        }
    };

    render() {
        let panes = [
            { menuItem: 'IES', render: () => <Tab.Pane><IESList clear={this.state.clear} submit={this.state.submit} {...this.state.form} {...this.props}/></Tab.Pane> },
            { menuItem: 'Unidade de Ensino', render: () => <Tab.Pane><UnidadeEnsinoList {...this.props}/></Tab.Pane> },
            { menuItem: 'Curso', render: () => <Tab.Pane><CursoList {...this.props}/></Tab.Pane> },
            { menuItem: 'Oferta', render: () => <Tab.Pane><OfertaList {...this.props}/></Tab.Pane> }
        ];
        /**
         * passar tabs para outro component de resultado ......
         * <Tab panes={panes} />
         */
        return (
            <div style={{padding: '2em'}}>
                <EmecHeader style={{margin: '2em'}} />
                <Header as='h3'>Pesquisar Instituição de Ensino</Header>
                <Container fluid style={{margin: '2em'}}>

                    <Segment>
                        <Form style={{marginTop: '2em'}}>
                            <Form.Group widths='equal'>
                                <Form.Input label='Código' placeholder='Código' value={this.state.form.codigo}
                                            name='codigo' onChange={this.handleChange} />
                                <Form.Input label='Nome da Instituição' placeholder='Nome' value={this.state.form.nome}
                                            name='nome' onChange={this.handleChange} />
                            </Form.Group>

                            <div style={{height: 40, display:'flex', justifyContent:'flex-end'}}>
                                <Button onClick={this.handleSubmit} primary disabled={this.state.form.invalid}>
                                    <Icon name='search' /> Pesquisar
                                </Button>
                                <Button onClick={this.handleClear}>
                                    Limpar
                                </Button>
                            </div>
                        </Form>
                    </Segment>

                    <Header as='h4'>Resultado</Header>
                    <Tab style={{marginTop: '2em'}} panes={panes} />
                </Container>
            </div>
        );
    }
}

export * from './EmecHeader';
export default Emec;