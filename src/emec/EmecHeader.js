import React, { Component } from 'react';
import { Header } from 'semantic-ui-react'
import EmecLogo from '../assets/emec_logo.gif';


class EmecHeader extends Component{

    render() {
        return (
            <div className='emec-header'>
                <a href="http://emec.mec.gov.br/" title="e-MEC - Sistema de Regulação do Ensino Superior">
                    <img src={EmecLogo} alt="e-MEC"/>
                </a>
                <Header as='h3' color='green' style={{transform:'translateY(-200%)'}}>
                    <Header.Content>
                        Instituições de Educação Superior e Cursos Cadastrados
                    </Header.Content>
                </Header>
            </div>
        );
    }
}

export default EmecHeader;