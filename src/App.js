import React, { Component } from 'react';
import { Container, Header} from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import SearchSisu from './search/SearchSisu';
import './App.css';


class App extends Component {

    render(){
        return (
            <div>
                <div className="sisu-header">
                    <div className="title-container">
                        <span className="title">SISU</span>
                        <div className="sub-title">
                            <span>SISTEMA DE SELEÇÃO UNIFICADA</span> <br/>
                            <span>2º PROCESSO SELETIVO DE 2017</span>
                        </div>
                    </div>
                    <div className="sub-header">
                        <span>
                            Pagina Inicial
                        </span>

                        <Link to="/emec" style={{textDecoration: 'none', color: 'white'}}>
                            <span style={{paddingLeft:'0.6em'}}>
                               Emec
                            </span>
                        </Link>
                    </div>
                </div>

                <div style={{padding: '2em'}}>
                    <Container fluid style={{margin: '2em'}}>
                        <Header as='h3'>Pesquisar Instituição de Ensino</Header>
                        <SearchSisu />
                    </Container>
                </div>
            </div>
        );
    }
}

export default App;
