import React, { Component } from 'react';
import { Button, Icon, Menu, Table, Loader } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import CursoService from './CursoService';


class CursoList extends Component{

    state = {
        loading: false,
        cursos: []
    };

    componentDidMount(){
        this.setState({loading: true});

        CursoService.list().then((data) => {
           this.setState({cursos: data, loading: false});
        });
    }

    handleEdit(id){
        this.props.history.push(`/cursos/${id}/editar`);
    }

    handleDelete(id){
        this.setState({loading: true});

        CursoService.delete(id).then(() => {
            return CursoService.list();
        }).then((data) => {
            this.setState({cursos: data, loading: false});
        });
    }

    renderCursoRow(curso, index){
        let dt_atualizacao = new Date(curso.dt_atualizacao).toString().replace(/GMT.+/, '');

        return (
            <Table.Row key={index}>
                <Table.Cell>{curso.no_curso}</Table.Cell>
                <Table.Cell>{curso.ds_turno}</Table.Cell>
                <Table.Cell>{curso.nu_carga_horaria}H</Table.Cell>
                <Table.Cell>{curso.co_modalidade_ensino}</Table.Cell>
                <Table.Cell>{dt_atualizacao}</Table.Cell>
                <Table.Cell>
                    <Button icon onClick={this.handleEdit.bind(this, curso.co_ies_curso)}>
                        <Icon name='edit'/>
                    </Button>
                    <Button icon onClick={this.handleDelete.bind(this, curso.co_ies_curso)}>
                        <Icon name='archive'/>
                    </Button>
                </Table.Cell>
            </Table.Row>
        );
    }

    render(){
        return (
            <div>
                {this.state.loading && (
                    <Loader style={{marginTop: '10em', marginBottom: '10em'}} active inline='centered' />
                )}

                {!this.state.loading && (
                <Table style={{marginTop: '2em'}} celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nome</Table.HeaderCell>
                            <Table.HeaderCell>Turno</Table.HeaderCell>
                            <Table.HeaderCell>Carga Horária</Table.HeaderCell>
                            <Table.HeaderCell>Modalidade</Table.HeaderCell>
                            <Table.HeaderCell>Atualização</Table.HeaderCell>
                            <Table.HeaderCell />
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.cursos.map((curso, index)=> this.renderCursoRow(curso, index))}
                    </Table.Body>

                    <Table.Footer fullWidth>
                        <Table.Row>
                            <Table.HeaderCell colSpan='7'>
                                <Menu pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='left chevron'/>
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='right chevron'/>
                                    </Menu.Item>
                                </Menu>

                                <Link to="/cursos/adicionar">
                                    <Button floated='right' icon labelPosition='left' primary size='small'>
                                        <Icon name='add'/> Adicionar Curso
                                    </Button>
                                </Link>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
                )}
            </div>
        );
    }
}

export default CursoList;




