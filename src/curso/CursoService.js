import axios from 'axios';
import URL from '../api';

const CURSO_URL = `${URL}/tb_gst_curso`;

class CursoService{

    static list(){
        return axios.get(CURSO_URL).then(response => response.data.elems.elem);
    }

    static delete(id){
        return axios.delete(`${CURSO_URL}/${id}`);
    }

    static save(unidade, update){
        let param = {};
        Object.keys(unidade).map((key) => {
            param[key + '_in'] = unidade[key];
        });
        unidade = param;

        if(update){
            return axios.put(CURSO_URL, unidade);
        }

        return axios.post(CURSO_URL, unidade);
    }

    static get(id){
        return axios.get(`${CURSO_URL}/${id}`).then(response => response.data.elems.elem);
    }
}

export default CursoService