import React, { Component } from 'react';
import { Form, Header, Container, Loader } from 'semantic-ui-react';
import update from 'immutability-helper';
import CursoService from './CursoService';
import EmecHeader from '../emec/EmecHeader';


class CursoForm extends Component{
    
    state = {
        loading: false,
        update: false,
        form: {
            co_termo_adesao: "",
            co_ies_endereco: "",
            co_ies_curso: "",
            co_turno: "",
            no_curso: "",
            ds_turno: "",
            co_curso: "",
            co_grau: "",
            ds_grau: "",
            nu_carga_horaria: "",
            nu_vagas_autorizadas: "",
            qt_semestre: "",
            ds_periodicidade: "",
            co_situacao_funcionamento: "",
            co_modalidade_ensino: "",
            st_ativo: "",
            co_dm_curso: "",
            dt_atualizacao: ""
        }
    };

    componentDidMount(){
        if(this.props.match && this.props.match.params.id){
            this.setState({loading: true, update: true});
            CursoService.get(this.props.match.params.id).then((data) => {
                this.setState({form: data, loading: false});
            });
        }
    }

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });
        this.setState({form: form});
    };

    handleSubmit = () => {
        this.setState({loading: true});
        CursoService.save(this.state.form, this.state.update).then((response) => {
            this.setState({loading: false});
            this.props.history.goBack();
        });
    };

    renderFormTitle(){
        if(this.state.update){
            return <Header size='large'>Atualizar Curso</Header>;
        }

        return <Header size='large'>Adicionar Curso</Header>;
    }

    render(){
        return (
            <div style={{padding: '2em'}}>
                <Container fluid style={{margin: '2em'}}>
                    <EmecHeader />
                    {this.renderFormTitle()}
                    {this.state.loading && (
                        <Loader style={{marginTop: '10em'}} active inline='centered' />
                    )}
                    <Form style={{marginTop: '2em'}} onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='Termo de adesão' placeholder='Termo'
                                        name='co_termo_adesao' onChange={this.handleChange}
                                        value={this.state.form.co_termo_adesao}/>
                            <Form.Input label='IES endereço' placeholder='Endereço'
                                        name='co_ies_endereco' onChange={this.handleChange}
                                        value={this.state.form.co_ies_endereco} />
                            <Form.Input label='IES curso' placeholder='Curso'
                                        name='co_ies_curso' onChange={this.handleChange}
                                        value={this.state.form.co_ies_curso}/>
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Nome do curso' placeholder='Nome'
                                        name='no_curso' onChange={this.handleChange}
                                        value={this.state.form.no_curso} />
                            <Form.Input label='Código do curso' placeholder='Código'
                                        name='co_curso' onChange={this.handleChange}
                                        value={this.state.form.co_curso} />
                            <Form.Input label='Código do turno' placeholder='Turno'
                                        name='co_turno' onChange={this.handleChange}
                                        value={this.state.form.co_turno} />
                            <Form.Input label='Descrição do turno' placeholder='Descrição'
                                        name='ds_turno' onChange={this.handleChange}
                                        value={this.state.form.ds_turno} />
                            <Form.Input label='Código do grau' placeholder='Código'
                                        name='co_grau' onChange={this.handleChange}
                                        value={this.state.form.co_grau} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Descrição do Grau' placeholder='Descrição'
                                        name='ds_grau' onChange={this.handleChange}
                                        value={this.state.form.ds_grau} />
                            <Form.Input label='Carga horária' placeholder='Carga'
                                        name='nu_carga_horaria' onChange={this.handleChange}
                                        value={this.state.form.nu_carga_horaria} />
                            <Form.Input label='Nº de vagas autorizadas' placeholder='Quantidade'
                                        name='nu_vagas_autorizadas' onChange={this.handleChange}
                                        value={this.state.form.nu_vagas_autorizadas} />
                            <Form.Input label='Quantidade de semestres' placeholder='Quantidade'
                                        name='qt_semestre' onChange={this.handleChange}
                                        value={this.state.form.qt_semestre} />
                            <Form.Input label='Descrição periodicidade' placeholder='Descrição'
                                        name='ds_periodicidade' onChange={this.handleChange}
                                        value={this.state.form.ds_periodicidade} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Situação do funcionamento' placeholder='Situação'
                                        name='co_situacao_funcionamento' onChange={this.handleChange}
                                        value={this.state.form.co_situacao_funcionamento} />
                            <Form.Input label='Modalidade de ensino' placeholder='Modalidade'
                                        name='co_modalidade_ensino' onChange={this.handleChange}
                                        value={this.state.form.co_modalidade_ensino} />
                            <Form.Input label='Código DM Curso' placeholder='Código'
                                        name='co_dm_curso' onChange={this.handleChange}
                                        value={this.state.form.co_dm_curso} />
                            <Form.Input label='Ativo' placeholder='Código'
                                        name='st_ativo' onChange={this.handleChange}
                                        value={this.state.form.st_ativo} />
                        </Form.Group>

                        <Form.Button primary>Salvar</Form.Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default CursoForm;