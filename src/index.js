import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';
import {
    HashRouter as Router,
    Route
} from 'react-router-dom';
import {routes} from './routes';


ReactDOM.render((
    <Router>
        <div>
            <div className="barra-brasil">
                <div className="wrapper-barra-brasil">
                    <div className="brasil-flag">
                        <a href="http://brasil.gov.br" className="link-barra">Brasil</a>
                    </div>
                    <span className="acesso-info">
                        <a href="http://brasil.gov.br/barra#acesso-informacao" className="link-barra">Acesso à informação</a>
                    </span>
                    <nav>
                        <ul className="list">
                            <li className="list-item first">
                                <a href="http://brasil.gov.br/barra#participe" className="link-barra">Participe</a>
                            </li>
                            <li className="list-item">
                                <a href="http://www.servicos.gov.br/?pk_campaign=barrabrasil" className="link-barra" id="barra-brasil-orgao">Serviços</a>
                            </li>
                            <li className="list-item">
                                <a href="http://www.planalto.gov.br/legislacao" className="link-barra">Legislação</a>
                            </li>
                            <li className="list-item last last-item">
                                <a href="http://brasil.gov.br/barra#orgaos-atuacao-canais" className="link-barra">Canais</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

            {routes.map((route, index) => {
                return <Route key={index} exact={route.exact} path={route.path} component={route.component} />;
              })
            }
        </div>
    </Router>
), document.getElementById('root'));

registerServiceWorker();
