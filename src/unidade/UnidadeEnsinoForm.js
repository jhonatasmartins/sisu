import React, { Component } from 'react';
import update from 'immutability-helper';
import { Form, Header, Loader, Container } from 'semantic-ui-react';
import UnidadeEnsinoService from './UnidadeEnsinoService';
import EmecHeader from '../emec/EmecHeader';


class UnidadeEnsinoForm extends Component{

    state = {
        update: false,
        loading: false,
        form : {
            co_ies: "",
            co_termo_adesao: "",
            co_ies_endereco: "",
            co_situacao_funcionamento: "",
            no_campus: "",
            ds_endereco: "",
            ds_complemento: "",
            nu_endereco: "",
            no_municipio: "",
            ds_bairro: "",
            sg_uf: "",
            nu_cep: "",
            st_sede: "",
            nu_telefone: "",
            nu_fax: "",
            co_dm_unidade_ensino: ""
        }
    };

    componentDidMount(){
        if(this.props.match && this.props.match.params.id){
            this.setState({loading: true, update: true});
            UnidadeEnsinoService.get(this.props.match.params.id).then((data) => {
                this.setState({form: data, loading: false});
            });
        }
    }

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });
        this.setState({form: form});
    };

    handleSubmit = () => {
        this.setState({loading: true});
        UnidadeEnsinoService.save(this.state.form, this.state.update).then((response) => {
            this.setState({loading: false});
            this.props.history.goBack();
        });
    };

    renderFormTitle(){
        if(this.state.update){
            return <Header size='large'>Atualizar Unidade de Ensino</Header>;
        }

        return <Header size='large'>Adicionar Unidade de Ensino</Header>;
    }

    render(){
        return (
            <div style={{padding: '2em'}}>
                <Container fluid style={{margin: '2em'}}>
                    <EmecHeader />
                    {this.renderFormTitle()}
                    {this.state.loading && (
                        <Loader style={{marginTop: '10em'}} active inline='centered' />
                    )}

                    <Form style={{marginTop: '2em'}} onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='Termo de adesão' placeholder='Termo'
                                        name='co_termo_adesao' onChange={this.handleChange}
                                        value={this.state.form.co_termo_adesao} />
                            <Form.Input label='IES endereço' placeholder='Endereço'
                                        name='co_ies_endereco' onChange={this.handleChange}
                                        value={this.state.form.co_ies_endereco}/>
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Nome do Campus' placeholder='Nome'
                                        name='no_campus' onChange={this.handleChange}
                                        value={this.state.form.no_campus} />
                            <Form.Input label='Endereço' placeholder='Endereço'
                                        name='ds_endereco' onChange={this.handleChange}
                                        value={this.state.form.ds_endereco} />
                            <Form.Input label='Complemento' placeholder='Complemento'
                                        name='ds_complemento' onChange={this.handleChange}
                                        value={this.state.form.ds_complemento} />
                            <Form.Input label='Nº' placeholder='10'
                                        name='nu_endereco' onChange={this.handleChange}
                                        value={this.state.form.nu_endereco} />
                            <Form.Input label='Município' placeholder='Município'
                                        name='no_municipio' onChange={this.handleChange}
                                        value={this.state.form.no_municipio} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Bairro' placeholder='Bairro'
                                        name='ds_bairro' onChange={this.handleChange}
                                        value={this.state.form.ds_bairro} />
                            <Form.Input label='UF' placeholder='UF'
                                        name='sg_uf' onChange={this.handleChange}
                                        value={this.state.form.sg_uf} />
                            <Form.Input label='CEP' placeholder='CEP'
                                        name='nu_cep' onChange={this.handleChange}
                                        value={this.state.form.nu_cep} />
                            <Form.Input label='Sede' placeholder='Sede'
                                        name='st_sede' onChange={this.handleChange}
                                        value={this.state.form.st_sede} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Telefone' placeholder='Telefone'
                                        name='nu_telefone' onChange={this.handleChange}
                                        value={this.state.form.nu_telefone} />
                            <Form.Input label='Fax' placeholder='Fax'
                                        name='nu_fax' onChange={this.handleChange}
                                        value={this.state.form.nu_fax} />
                            <Form.Input label='Situação de Funcionamento' placeholder='Situação'
                                        name='co_situacao_funcionamento' onChange={this.handleChange}
                                        value={this.state.form.co_situacao_funcionamento} />
                            <Form.Input label='Código DM unidade de ensino' placeholder='Código'
                                        name='co_dm_unidade_ensino' onChange={this.handleChange}
                                        value={this.state.form.co_dm_unidade_ensino} />
                        </Form.Group>

                        <Form.Button primary>Salvar</Form.Button>
                    </Form>
                </Container>
            </div>
        );
    }


}

export default UnidadeEnsinoForm;