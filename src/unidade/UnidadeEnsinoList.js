import React, { Component } from 'react';
import {
    Button, Icon, Menu, Table,
    Loader
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import UnidadeEnsinoService from './UnidadeEnsinoService';


class UnidadeEnsinoList extends Component{

    state = {
       unidades: [],
       loading: false
    };

    componentDidMount(){
        this.setState({loading: true});

        UnidadeEnsinoService.list().then((data) => {
            this.setState({unidades: data, loading: false});
        });
    }

    handleEdit(id){
        this.props.history.push(`/unidades/${id}/editar`);
    }

    handleDelete(id){
        this.setState({loading: true});

        UnidadeEnsinoService.delete(id).then(() => {
           return UnidadeEnsinoService.list();
        }).then((data) => {
            this.setState({unidades: data, loading: false});
        });
    }

    renderUnidadeRow(unidade, index){
        let dt_atualizacao = new Date(unidade.dt_atualizacao).toString().replace(/GMT.+/, '');
        return(
           <Table.Row key={index}>
             <Table.Cell>{unidade.no_campus}</Table.Cell>
             <Table.Cell>{unidade.ds_endereco}</Table.Cell>
             <Table.Cell>{unidade.no_municipio}</Table.Cell>
             <Table.Cell>{unidade.ds_bairro}</Table.Cell>
             <Table.Cell>{unidade.nu_telefone}</Table.Cell>
             <Table.Cell>{dt_atualizacao}</Table.Cell>
             <Table.Cell>
                <Button icon onClick={this.handleEdit.bind(this, unidade.co_ies)}>
                    <Icon name='edit'/>
                </Button>
                <Button icon onClick={this.handleDelete.bind(this, unidade.co_ies)}>
                    <Icon name='archive'/>
                </Button>
             </Table.Cell>
           </Table.Row>
        );
    }

    render(){
        return (
            <div>
                {this.state.loading && (
                    <Loader style={{marginTop: '10em', marginBottom: '10em'}} active inline='centered' />
                )}

                {!this.state.loading &&
                (<Table style={{marginTop: '2em'}} celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Nome</Table.HeaderCell>
                            <Table.HeaderCell>Endereço</Table.HeaderCell>
                            <Table.HeaderCell>Município</Table.HeaderCell>
                            <Table.HeaderCell>Bairro</Table.HeaderCell>
                            <Table.HeaderCell>Telefone</Table.HeaderCell>
                            <Table.HeaderCell>Última Atualização</Table.HeaderCell>
                            <Table.HeaderCell />
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.unidades.map((unidade, index) => {
                            return this.renderUnidadeRow(unidade, index);
                        })}
                    </Table.Body>

                    <Table.Footer fullWidth>
                        <Table.Row>
                            <Table.HeaderCell colSpan='7'>
                                <Menu pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='left chevron'/>
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='right chevron'/>
                                    </Menu.Item>
                                </Menu>

                                <Link to="/unidades/adicionar">
                                    <Button floated='right' icon labelPosition='left' primary size='small'>
                                        <Icon name='add'/> Adicionar Unidade de Ensino
                                    </Button>
                                </Link>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>
                )}
            </div>
        );
    }
}

export default UnidadeEnsinoList;




