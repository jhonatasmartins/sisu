import axios from 'axios';
import URL from '../api';

const UNIDADE_URL = `${URL}/tb_gst_unidade_ensino`;

class UnidadeEnsinoService{

    static list(){
        return axios.get(UNIDADE_URL).then(response => response.data.elems.elem);
    }

    static delete(id){
        return axios.delete(`${UNIDADE_URL}/${id}`);
    }

    static save(unidade, update){
        if(update){
            return axios.put(UNIDADE_URL, unidade);
        }

        return axios.post(UNIDADE_URL, unidade);
    }

    static get(id){
        return axios.get(`${UNIDADE_URL}/${id}`).then(response => response.data.elems.elem);
    }

}

export default UnidadeEnsinoService