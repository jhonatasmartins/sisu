
import App from './App';
import Emec from './emec';
import CursoForm from './curso/CursoForm';
import OfertaForm from './oferta/OfertaForm';
import UnidadeEnsinoForm from './unidade/UnidadeEnsinoForm';
import IESForm from './ies/IESForm';


const routes = [
    {
        exact: true,
        path: '/',
        component: App
    },
    {
        exact: true,
        path: '/emec',
        component: Emec
    },
    {
        path: '/cursos/adicionar',
        component: CursoForm
    },
    {
        path: '/cursos/:id/editar',
        component: CursoForm
    },
    {
        path: '/ofertas/adicionar',
        component: OfertaForm
    },
    {
        path: '/ofertas/:id/editar',
        component: OfertaForm
    },
    {
        path: '/unidades/adicionar',
        component: UnidadeEnsinoForm
    },
    {
        path: '/unidades/:id/editar',
        component: UnidadeEnsinoForm
    },
    {
        path: '/ies/adicionar',
        component: IESForm
    },
    {
        path: '/ies/:id/editar',
        component: IESForm
    }
];

export {routes};