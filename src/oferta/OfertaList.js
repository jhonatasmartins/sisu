import React, { Component } from 'react';
import { Button, Icon, Menu, Table, Loader } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import OfertaService from './OfertaService';


class OfertaList extends Component{

    state = {
        loading: false,
        ofertas: []
    };

    componentDidMount(){
        this.setState({loading: true});

        OfertaService.list().then((data) => {
            this.setState({ofertas: data, loading: false});
        });
    }

    handleEdit(id){
        this.props.history.push(`/ofertas/${id}/editar`);
    }

    handleDelete(id){
        this.setState({loading: true});

        OfertaService.delete(id).then(() => {
            return OfertaService.list();
        }).then((data) => {
            this.setState({ofertas: data, loading: false});
        });
    }

    renderOfertaRow(oferta, index){

        return (
            <Table.Row key={index}>
                <Table.Cell>{oferta.co_oferta}</Table.Cell>
                <Table.Cell>{oferta.co_ies_curso}</Table.Cell>
                <Table.Cell>{oferta.nu_carga_horaria}H</Table.Cell>
                <Table.Cell>{oferta.nu_vagas_autorizadas}</Table.Cell>
                <Table.Cell>
                    <Button icon onClick={this.handleEdit.bind(this, oferta.co_oferta)}>
                        <Icon name='edit'/>
                    </Button>
                    <Button icon onClick={this.handleDelete.bind(this, oferta.co_oferta)}>
                        <Icon name='archive'/>
                    </Button>
                </Table.Cell>
            </Table.Row>
        );
    }

    render(){
        return (
            <div>
                {this.state.loading && (
                    <Loader style={{marginTop: '10em', marginBottom: '10em'}} active inline='centered' />
                )}

                {!this.state.loading && (
                <Table style={{marginTop: '2em'}} celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Código</Table.HeaderCell>
                            <Table.HeaderCell>Curso</Table.HeaderCell>
                            <Table.HeaderCell>Carga Horária</Table.HeaderCell>
                            <Table.HeaderCell>Vagas autorizadas</Table.HeaderCell>
                            <Table.HeaderCell />
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.ofertas.map((oferta, index)=> this.renderOfertaRow(oferta, index))}
                    </Table.Body>

                    <Table.Footer fullWidth>
                        <Table.Row>
                            <Table.HeaderCell colSpan='6'>
                                <Menu pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='left chevron'/>
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='right chevron'/>
                                    </Menu.Item>
                                </Menu>

                                <Link to="/ofertas/adicionar">
                                    <Button floated='right' icon labelPosition='left' primary size='small'>
                                        <Icon name='add'/> Adicionar Oferta
                                    </Button>
                                </Link>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>)}
            </div>
        );
    }
}

export default OfertaList;




