import axios from 'axios';
import URL from '../api';


const OFERTA_URL = `${URL}/tb_gst_oferta`;

class OfertaService{

    static list(){
        return axios.get(OFERTA_URL).then(response => response.data.elems.elem);
    }

    static delete(id){
        return axios.delete(`${OFERTA_URL}/${id}`);
    }

    static save(unidade, update){
        let param = {};
        Object.keys(unidade).map((key) => {
            param[key + '_in'] = unidade[key];
        });
        unidade = param;

        if(update){
            return axios.put(OFERTA_URL, unidade);
        }

        return axios.post(OFERTA_URL, unidade);
    }

    static get(id){
        return axios.get(`${OFERTA_URL}/${id}`).then(response => response.data.elems.elem);
    }
}

export default OfertaService