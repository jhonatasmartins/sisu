import React, { Component } from 'react';
import { Form, Header, Loader, Container } from 'semantic-ui-react';
import update from 'immutability-helper';
import OfertaService from './OfertaService';
import EmecHeader from '../emec/EmecHeader';


class OfertaForm extends Component{

    state = {
        loading: false,
        update: false,
        form: {
            co_ies_curso:"",
            co_oferta:"",
            co_usuario:"",
            st_situacao:"",
            dt_inclusao:"",
            co_termo_adesao:"",
            co_ies_endereco:"",
            co_turno:"",
            nu_carga_horaria:"",
            nu_vagas_autorizadas:"",
            co_dm_curso:""
        }
    };

    componentDidMount(){
        if(this.props.match && this.props.match.params.id){
            this.setState({loading: true, update: true});
            OfertaService.get(this.props.match.params.id).then((data) => {
                this.setState({form: data, loading: false});
            });
        }
    }

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });
        this.setState({form: form});
    };

    handleSubmit = () => {
        this.setState({loading: true});
        OfertaService.save(this.state.form, this.state.update).then((response) => {
            this.setState({loading: false});
            this.props.history.goBack();
        });
    };

    renderFormTitle(){
        if(this.state.update){
            return <Header size='large'>Atualizar Oferta</Header>;
        }

        return <Header size='large'>Adicionar Oferta</Header>;
    }

    render(){
        return (
            <div style={{padding: '2em'}}>
                <Container fluid style={{margin: '2em'}}>
                    <EmecHeader />
                    {this.renderFormTitle()}
                    {this.state.loading && (
                        <Loader style={{marginTop: '10em'}} active inline='centered' />
                    )}
                    <Form style={{marginTop: '2em'}} onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='IES endereço' placeholder='Endereço'
                                        name='co_ies_endereco' onChange={this.handleChange}
                                        value={this.state.form.co_ies_endereco} />
                            <Form.Input label='IES curso' placeholder='Curso'
                                        name='co_ies_curso' onChange={this.handleChange}
                                        value={this.state.form.co_ies_curso} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Código DM curso' placeholder='Código'
                                        name='co_dm_curso' onChange={this.handleChange}
                                        value={this.state.form.co_dm_curso} />
                            <Form.Input label='Código do turno' placeholder='Turno'
                                        name='co_turno' onChange={this.handleChange}
                                        value={this.state.form.co_turno} />
                            <Form.Input label='Código do usuário' placeholder='Usuário'
                                        name='co_usuario' onChange={this.handleChange}
                                        value={this.state.form.co_usuario}  />
                        </Form.Group>


                        <Form.Group widths='equal'>
                            <Form.Input label='Carga horária' placeholder='Carga'
                                        name='nu_carga_horaria' onChange={this.handleChange}
                                        value={this.state.form.nu_carga_horaria}  />
                            <Form.Input label='Nº de vagas autorizadas' placeholder='Quantidade'
                                        name='nu_vagas_autorizadas' onChange={this.handleChange}
                                        value={this.state.form.nu_vagas_autorizadas} />
                        </Form.Group>

                        <Form.Button primary>Salvar</Form.Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default OfertaForm;