import React, { Component } from 'react';
import { Header, Table, Segment, Icon, Form, Button, Modal, List, Loader } from 'semantic-ui-react'
import update from 'immutability-helper';
import IESService from '../ies/IESService';


class SearchSisu extends Component{
    state = {
        nomeInstituicao: "",
        modalClosed: true,
        loading: false,
        data : [],
        searched : false,
        form: {
            codigo: "",
            nome: "",
            invalid: true
        },
        unidades: [{
            "ds_bairro": "Aguas Claras",
            "nu_telefone": "44443020",
            "no_municipio": "Brasilia",
            "nu_cep": "70000",
            "no_campus": "Faculdade Marilia",
            "nu_endereco": "1",
            "co_ies_endereco": "1",
            "co_termo_adesao": "1",
            "co_situacao_funcionamento": "1",
            "nu_fax": "33331002",
            "dt_atualizacao": "2017-01-01Z",
            "sg_uf": "DF",
            "co_ies": "1",
            "ds_complemento": "Teste Faculdade",
            "st_sede": "1",
            "co_dm_unidade_ensino": "1",
            "ds_endereco": "Rua ABC",
            "cursos": [
                {
                    "co_turno": "1",
                    "no_curso": "Direito",
                    "co_modalidade_ensino": "1",
                    "co_curso": "1",
                    "co_dm_curso": "1",
                    "nu_vagas_autorizadas": "30",
                    "co_grau": "1",
                    "nu_carga_horaria": "6",
                    "co_ies_endereco": "1",
                    "co_termo_adesao": "1",
                    "st_ativo": "1",
                    "ds_periodicidade": "Matutino",
                    "co_situacao_funcionamento": "1",
                    "dt_atualizacao": "2017-01-01Z",
                    "ds_grau": "Grau 1",
                    "co_ies_curso": "1",
                    "ds_turno": "Direito",
                    "qt_semestre": "2",
                    "co_ies": "1"
                }
            ]
        }]
    };

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });

        form.invalid = !form.codigo && !form.nome;
        this.setState({form: form});
    };

    handleSubmit = () => {
        let {codigo, nome} = this.state.form;
        this.setState({searched: true, loading: true});

        IESService.search(codigo, nome).then((data) => {
           this.setState({data: data || [], loading: false});
        });
    };

    handleCloseModal = () => {
        this.setState({modalClosed:true})
    };

    handleView = (ies) => {
        IESService.all(ies.co_ies, ies.co_termo_adesao).then(data => {
            this.setState({nomeInstituicao: ies.no_ies, unidades: data, modalClosed: false});
            return data;
        }).then((data) => {

            return Promise.all(data.map((unidade) => {
                return IESService.cursos(unidade.co_ies_endereco, unidade.co_termo_adesao).then((cursos) => {
                    unidade.cursos = cursos;
                })
            })).then(() => {
                return data;
            });
        }).then((data) => {
            this.setState({unidades: data});
        })
    };

    render() {
        return (
            <div>
                <Segment>
                    <Form style={{marginTop: '2em'}} onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='Código' placeholder='Código'
                                        name='codigo' onChange={this.handleChange} />
                            <Form.Input label='Nome da Instituição' placeholder='Nome'
                                        name='nome' onChange={this.handleChange} />
                        </Form.Group>

                        <div style={{height: 40}}>
                            <Form.Button floated='right' primary disabled={this.state.form.invalid}>
                                <Icon name='search' /> Pesquisar
                            </Form.Button>
                        </div>
                    </Form>
                </Segment>


                {this.state.searched && (
                    <Table style={{marginTop: '2em'}} celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Código</Table.HeaderCell>
                                <Table.HeaderCell>Nome</Table.HeaderCell>
                                <Table.HeaderCell>Sigla</Table.HeaderCell>
                                <Table.HeaderCell>Sítio</Table.HeaderCell>
                                <Table.HeaderCell>Telefone</Table.HeaderCell>
                                <Table.HeaderCell>Última Atualização</Table.HeaderCell>
                                <Table.HeaderCell />
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {(this.state.searched && !this.state.loading) && this.state.data.length === 0 && (<Table.Row>
                                <Table.Cell colSpan='6' textAlign='center' >Nenhum registro encontrado!</Table.Cell>
                            </Table.Row>)}
                            {this.state.loading && (
                                <Table.Row>
                                    <Table.Cell colSpan='6' textAlign='center' >
                                        <Loader style={{marginTop: '10em', marginBottom: '10em'}} active inline='centered' />
                                    </Table.Cell>
                                </Table.Row>
                            )}

                            {this.state.data.map((ies, index) => {
                                return <Table.Row key={index}>
                                    <Table.Cell>{ies.co_ies}</Table.Cell>
                                    <Table.Cell>{ies.no_ies}</Table.Cell>
                                    <Table.Cell>{ies.sg_ies}</Table.Cell>
                                    <Table.Cell>{ies.ds_sitio}</Table.Cell>
                                    <Table.Cell>{ies.nu_telefone}</Table.Cell>
                                    <Table.Cell>{ies.dt_atualizacao}</Table.Cell>
                                    <Table.Cell>
                                        <Button icon onClick={this.handleView.bind(this, ies)}>
                                            <Icon name='eye'/>
                                        </Button>
                                    </Table.Cell>
                                </Table.Row>;
                            })}

                        </Table.Body>
                    </Table>
                )}

                <Modal open={!this.state.modalClosed} closeIcon={true} onClose={this.handleCloseModal}>
                    <Modal.Header>Instituição - {this.state.nomeInstituicao}</Modal.Header>
                    <Modal.Content image>
                        <Modal.Description>
                            <Header>Unidades de Ensino</Header>

                            <List divided>
                                {this.state.unidades.map((item, index) => {
                                    return <List.Item key={index}>
                                        <List.Icon name='university' />
                                        <List.Content>
                                            <List.Header as='a'>{item.no_campus}</List.Header>
                                            <List.Description as='a'> {item.ds_bairro} - {item.no_municipio}</List.Description>
                                            <List.List>
                                                {item.cursos.map((curso, cursoIndex) => {
                                                    return  <List.Item key={cursoIndex}>
                                                        <List.Icon name='student' />
                                                        <List.Content>
                                                            <List.Header>{curso.no_curso} - {curso.ds_periodicidade} - {curso.nu_carga_horaria}H</List.Header>
                                                            <List.Description>Vagas: {curso.nu_vagas_autorizadas}</List.Description>
                                                        </List.Content>
                                                    </List.Item>
                                                })}
                                            </List.List>
                                        </List.Content>
                                    </List.Item>
                                })}
                            </List>

                        </Modal.Description>
                    </Modal.Content>
                </Modal>

            </div>
        );
    }
}

export default SearchSisu;