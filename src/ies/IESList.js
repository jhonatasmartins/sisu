import React, { Component } from 'react';
import { Button, Icon, Menu, Table, Loader } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import IESService from './IESService';


class IESList extends Component{

    state = {
        submit: false,
        loading: false,
        ies: []
    };

    componentDidMount(){
        this.setState({loading: true});

        IESService.list().then((data) => {
            this.setState({ies: data, loading: false});
        });
    }

    componentWillReceiveProps(nextProps){
        let {codigo, nome, submit, clear} = nextProps;

        if(submit === !this.state.submit){
            this.setState({submit});

            if(submit){
                this.setState({submit, codigo, nome, loading: true});
                IESService.search(codigo, nome).then((response) => {
                    this.setState({ies: response.data, loading: false});
                });
            }else if (clear){
                IESService.list().then((response) => {
                    this.setState({ies: response.data, loading: false});
                });
            }
        }
    }

    handleEdit(id){
        this.props.history.push(`/ies/${id}/editar`);
    }

    handleDelete(termo_adesao, id){
        this.setState({loading: true});

        IESService.delete(termo_adesao, id).then(() => {
            return IESService.list();
        }).then((data) => {
            this.setState({ies: data, loading: false});
        });
    }

    renderIESRow(ies, index){
        return (
            <Table.Row key={index}>
                <Table.Cell>{ies.co_ies}</Table.Cell>
                <Table.Cell>{ies.no_ies}</Table.Cell>
                <Table.Cell>{ies.ds_sitio}</Table.Cell>
                <Table.Cell>{ies.ds_email}</Table.Cell>
                <Table.Cell>
                    <Button icon onClick={this.handleEdit.bind(this, ies.co_ies)}>
                        <Icon name='edit'/>
                    </Button>
                    <Button icon onClick={this.handleDelete.bind(this, ies.co_termo_adesao, ies.co_ies)}>
                        <Icon name='archive'/>
                    </Button>
                </Table.Cell>
            </Table.Row>
        );
    }

    render(){
        return (
            <div>
                {this.state.loading && (
                    <Loader style={{marginTop: '10em', marginBottom: '10em'}} active inline='centered' />
                )}

                {!this.state.loading && (
                <Table style={{marginTop: '2em'}} celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Código IES</Table.HeaderCell>
                            <Table.HeaderCell>Nome IES</Table.HeaderCell>
                            <Table.HeaderCell>Sítio</Table.HeaderCell>
                            <Table.HeaderCell>Email</Table.HeaderCell>
                            <Table.HeaderCell />
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {this.state.ies.map((ies, index)=> this.renderIESRow(ies, index))}
                    </Table.Body>

                    <Table.Footer fullWidth>
                        <Table.Row>
                            <Table.HeaderCell colSpan='6'>
                                <Menu pagination>
                                    <Menu.Item as='a' icon>
                                        <Icon name='left chevron'/>
                                    </Menu.Item>
                                    <Menu.Item as='a'>1</Menu.Item>
                                    <Menu.Item as='a'>2</Menu.Item>
                                    <Menu.Item as='a'>3</Menu.Item>
                                    <Menu.Item as='a'>4</Menu.Item>
                                    <Menu.Item as='a' icon>
                                        <Icon name='right chevron'/>
                                    </Menu.Item>
                                </Menu>

                                <Link to="/ies/adicionar">
                                    <Button floated='right' icon labelPosition='left' primary size='small'>
                                        <Icon name='add'/> Adicionar IES
                                    </Button>
                                </Link>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>)}
            </div>
        );
    }
}

export default IESList;




