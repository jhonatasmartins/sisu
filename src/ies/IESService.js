import axios from 'axios';
import URL from '../api';


const IES_URL = `${URL}/tb_gst_ies`;


class IESService{

    static list(){
        return axios.get(IES_URL).then(response => response.data.elems.elem);
    }

    static delete(termo_adesao, id){
        return axios.delete(`${IES_URL}/${termo_adesao}/${id}`);
    }

    static save(unidade, update){
        let param = {};
        Object.keys(unidade).map((key) => {
            param[key + '_in'] = unidade[key];
        });
        unidade = param;

        if(update){
            return axios.put(IES_URL, unidade);
        }

        return axios.post(IES_URL, unidade);
    }

    static get(id){
        return axios.get(`${IES_URL}/${id}`).then(response => response.data.elems.elem);
    }

    static search(codigo = 0, nome){
        let params = {
            "co_ies_in": codigo,
            "no_ies_in": nome == '' ? null : nome
        };
        return axios.post(`${URL}/tb_gst_iesByCodigoOuDescricao`, params).then(response => {
            let data = response.data.elems.elem;
            return Array.isArray(data) ? data : [data];
        });
    }

    static all(codigo, termo){
        return axios.get(`${URL}/tb_gst_unidade_ensinoByInstituicoes/${termo}/${codigo}`).then(response => {
            let data = response.data.elems.elem;
            return Array.isArray(data) ? data : [data];
        }).then(unidades => {
           unidades.map((unidade) => {
                unidade.cursos = [];
            });

            return unidades;
        });
    }

    static cursos(codigo, termo){
        return axios.get(`${URL}/tb_gst_cursoByEndereco/${codigo}/${termo}`).then(response => {
            let data = response.data.elems.elem;
            return Array.isArray(data) ? data : [data];
        });
    }
}

export default IESService