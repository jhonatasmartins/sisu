import React, { Component } from 'react';
import { Form, Header, Container } from 'semantic-ui-react';
import update from 'immutability-helper';
import IESService from './IESService';
import EmecHeader from '../emec/EmecHeader';



class IESForm extends Component{

    state = {
        update: false,
        form: {
            co_ies:"",
            sg_ies:"",
            no_ies:"",
            ds_sitio:"",
            ds_email:"",
            nu_telefone:"",
            nu_fax:"",
            co_dm_ies: "",
            co_termo_adesao: "",
            co_situacao_funcionamento: "",
            dt_atualizacao: "2017-08-08"
        }
    };

    componentDidMount(){
        if(this.props.match && this.props.match.params.id){
            this.setState({loading: true, update: true});
            IESService.get(this.props.match.params.id).then((data) => {
                this.setState({form: data, loading: false});
            });
        }
    }

    handleChange = (e, { name, value }) =>{
        let form = update(this.state.form, {
            [name]: {$set: value}
        });
        this.setState({form: form});
    };

    handleSubmit = () => {
        this.setState({loading: true});
        IESService.save(this.state.form, this.state.update).then((response) => {
            this.setState({loading: false});
            this.props.history.goBack();
        });
    };

    renderFormTitle(){
        if(this.state.update){
            return <Header size='large'>Atualizar IES</Header>;
        }
        return <Header size='large'>Adicionar IES</Header>;
    }

    render(){
        return (
            <div style={{padding: '2em'}}>
                <Container fluid style={{margin: '2em'}}>
                    <EmecHeader />
                    {this.renderFormTitle()}
                    <Form style={{marginTop: '2em'}} onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='Termo de adesão' placeholder='Termo'
                                        name='co_termo_adesao' onChange={this.handleChange}
                                        value={this.state.form.co_termo_adesao} />
                            <Form.Input label='Código IES' placeholder='Código'
                                        name='co_ies' onChange={this.handleChange}
                                        value={this.state.form.co_ies} />
                            <Form.Input label='Nome IES' placeholder='Nome'
                                        name='no_ies' onChange={this.handleChange}
                                        value={this.state.form.no_ies} />
                            <Form.Input label='SG IES' placeholder='Sigla'
                                        name='sg_ies' onChange={this.handleChange}
                                        value={this.state.form.sg_ies} />
                        </Form.Group>

                        <Form.Group widths='equal'>
                            <Form.Input label='Sítio' placeholder='Sítio'
                                        name='ds_sitio' onChange={this.handleChange}
                                        value={this.state.form.ds_sitio} />
                            <Form.Input label='Email' placeholder='Email'
                                        name='ds_email' onChange={this.handleChange}
                                        value={this.state.form.ds_email} />
                            <Form.Input label='Telefone' placeholder='Telefone'
                                        name='nu_telefone' onChange={this.handleChange}
                                        value={this.state.form.nu_telefone} />
                        </Form.Group>


                        <Form.Group widths='equal'>
                            <Form.Input label='Fax' placeholder='Fax'
                                        name='nu_fax' onChange={this.handleChange}
                                        value={this.state.form.nu_fax} />
                            <Form.Input label='Situação Funcionamento' placeholder='Situação'
                                        name='co_situacao_funcionamento' onChange={this.handleChange}
                                        value={this.state.form.co_situacao_funcionamento} />
                            <Form.Input label='Código DM IES' placeholder='Código'
                                        name='co_dm_ies' onChange={this.handleChange}
                                        value={this.state.form.co_dm_ies} />
                        </Form.Group>

                        <Form.Button primary>Salvar</Form.Button>
                    </Form>
                </Container>
            </div>
        );
    }
}

export default IESForm;