var express = require('express'),
    path    = require('path'),
    app     = express();


var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

app.use(express.static(path.resolve(__dirname + '/build/')));

app.listen(port, ip);
console.log('Server running on http://%s:%s', ip, port);

module.exports = app ;


